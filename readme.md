# Production test technique

Candidature Asobo Studio 2020 - Nicolas Violette

## Affichage
Ouvrir le fichier index.html dans un navigateur Chrome ou Firefox récent (je n'ai testé que sur ces deux-là). Mettez le navigateur en plein écran avec F11 pour encore plus d'effet wouaouh !

## Compilation
Chaque fichier Sass doit être compilé, le fichier .css résultant doit être dans le même dossier sur le .sass originel.

## Choix de production particuliers
Dans le fichier des consignes, il était indiqué "default/over/click" pour les tuiles centrales, j'ai donc considéré que l'affichage blanc de la preview était l'état "hover".

Naturellement, lors d'un clique sur une tuile, la page est sensé appeler "la suite", quelle qu'elle soit : le chargement du niveau par exemple. J'ai considéré que ça suffirait d'afficher l'état "click" et qu'il n'était pas forcément pertinent d'effectuer des actions sur l'interface après celui-ci.

Pour l'animation de chargement globale, les tuiles sont déjà très imposantes, et j'ai jugé plus esthétique d'avoir un simple fondu sur le menu du haut, avec un petit délai volontaire avant l'affichage desdites tuiles. J'espère que vous serez du même avis =)

Sur le fichier de preview, il y avait beaucoup d’inconsistances dans les espacements des différentes éléments (par exemple, entres les items du menu principal, dans la marge intérieur des tuiles noir vs. blanches, dans l'espacement autours des tuiles des différentes lignes, un espace superflu entre le fond noir du texte des tuiles et la bordure de l'image, ...). J'ai considéré qu'ils étaient l'artefact de l'outil utilisé pour générer l'image, et qu'il fallait mieux avoir une interface finale consistante.