
// Création d'un objet qui gère toutes les données et la logique de navigation
// Remplace potentiellement des connexions à d'autres services, une BDD, que sais-je

class FakeDatas {
	constructor() {

		// Données extraites de la Preview

		this.menu = [
			{
				label: "Page1",
				subMenu: [
					{ label: "Sub Page A", current: true},
					{ label: "Sub Page B" },
					{ label: "Sub Page C" },
					{ label: "Sub Page D" },
				],
			},
			{
				label: "Page 2",
				subMenu: [
					{ label: "Sub Page 1" },
					{ label: "Sub Page 2", larger: true, current: true },
					{ label: "Sub Page 3" },
				],
				current: true,
			},
			{
				label: "Page 3",
				subMenu: [
					{ label: "Sub Page E", larger: true, current: true},
					{ label: "Sub Page F", larger: true },
				],
			},
			{
				label: "Page 4",
				subMenu: [
					{ label: "Sub Page 4", current: true},
					{ label: "Sub Page 5", larger: true},
					{ label: "Sub Page 6" },
				],
			},
		]

		this.tiles = [
			{
				title: "Lorem upsum",
				image: "Tile_image_1.png",
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat....",
				larger: true,
				current: true,
			},
			{
				title: "<strong>Dolor</strong> sit",
				image: "Tile_image_2.png",
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo ...",
			},
			{
				title: "<strong>Elit</strong> sed diam",
				image: "Tile_image_3.png",
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat...",
			},
			{
				title: "<strong>Magna</strong> erat",
				image: "Tile_image_4.png",
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud...",
			},
			{
				title: "<strong>Consectetuer</strong> nibh",
				image: "Tile_image_5.png",
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...",
			},
		]
	}

	// Méthodes de navigations

	setCurrentMenu (nextMenu) {
		let currentMenu = window.fakeDataManager.menu.filter(e => e.current)[0]
		if (currentMenu !== nextMenu) {
			currentMenu.current = false
			window.fakeDataManager.menu.filter(e => e === nextMenu)[0].current = true
			document.body.querySelector("asobo-home").update_content()
		}
	}

	setCurrentSubMenu (nextSubMenu) {
		let currentMenu = window.fakeDataManager.menu.filter(e => e.current)[0]
		let currentSubMenu = currentMenu.subMenu.filter(e => e.current)[0]
		if (currentSubMenu !== nextSubMenu) {
			currentSubMenu.current = false
			currentMenu.subMenu.filter(e => e === nextSubMenu)[0].current = true
			document.body.querySelector("asobo-home").update_content()
		}
	}
}