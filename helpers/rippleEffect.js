
// Fonction pour ajouter un effet de bulle sur les éléments cliqués
function addRippleEffect(htmlElement) {
	htmlElement.addEventListener("mousedown", evt => {
		if (htmlElement.rippleEffectElement === undefined) {
			// Création d'un composant Ripple et positionnement sur l'élement + la souris
			let ripple = document.createElement("ripple-effect")
			htmlElement.rippleEffectElement = ripple
			htmlElement.appendChild(ripple)
			let boundingBox = htmlElement.getBoundingClientRect()
			ripple.style.top = boundingBox.top + "px"
			ripple.style.left = boundingBox.left + "px"
			ripple.style.width = boundingBox.width + "px"
			ripple.style.height = boundingBox.height + "px"
			// Positionnement de la bulle relative le wrapper, d'où adaptation des coordonées
			// de la souris qui sont relative à la fenêtre
			ripple.setCirclePos(evt.clientX - boundingBox.left, evt.clientY - boundingBox.top)

			let removable = false
			let toRemove = false

			// Préparation à la supression quand relachement du click
			let remove = () => {
				if (removable) {
					if (htmlElement.rippleEffectElement !== undefined) {
						htmlElement.removeChild(htmlElement.rippleEffectElement)
						htmlElement.rippleEffectElement = undefined
					}
					removable = false
				} else {
					toRemove = true
				}
			}
			
			// Empêcher la supression trop rapide...
			window.setTimeout(() => {
				removable = true
				// ... mais si la supression aurait du avoir lieu, la relancer
				if (toRemove) {
					toRemove = false
					remove()
				}
			}, 400)
			
			window.addEventListener("mouseup", remove)
			ripple.addEventListener("mouseleave", remove)
		}
	})
}

class RippleEffect extends HTMLElement {
	constructor() {
			super()
			this.circle = undefined
	}

	connectedCallback() {
		this.update_content()
	}

	update_content() {
		this.circle = document.createElement("div")
		this.circle.className = "circle"
		this.appendChild(this.circle)
	}
	
	setCirclePos(x, y) {
		this.circle.style.left = x + "px"
		this.circle.style.top = y + "px"
	}
}

window.customElements.define('ripple-effect', RippleEffect)