class HomeTile extends HTMLElement {
	constructor() {
			super()
			this.label = undefined
			this.titleLabel = undefined
			this.image = undefined
			this.description = undefined
			this.imgElt = undefined
	}

	connectedCallback() {
		this.update_content()
		addRippleEffect(this)
		// Gestion d'un parallaxe stylé !
		this.addEventListener("mousemove", evt => {
			let boundingBox = this.getBoundingClientRect()
			// Adaptation locale des coordonées de la souris qui sont relative à la fenêtre
			let localMouseX = evt.clientX - boundingBox.left
			let localMouseY = evt.clientY - boundingBox.top
			let ratioX = localMouseX / (2 * boundingBox.width)
			let ratioY = localMouseY /  (2 * boundingBox.height)
			const distanceMaxParralaxe = 10
			let parralaXe = ratioX * (2 * distanceMaxParralaxe)
			let parralaYe = ratioY * (2 * distanceMaxParralaxe)
			this.imgElt.style.top = (-parralaYe) + "px"
			this.imgElt.style.left = (-parralaXe) + "px"
		})
		this.addEventListener("mouseleave", evt => {
			this.imgElt.style.top = "0px"
			this.imgElt.style.left = "0px"
		})
	}

	update_content() {
		this.innerHTML = "";
		this.innerHTML = `
			<header class='tile-title'>${this.titleLabel}</header>
			<div class='tile-imgWrapper'><img class='tile-image' src="ressources/${this.image}"></div>
			<p class='tile-description'>${this.description}</p>
		`
		this.imgElt = this.getElementsByClassName('tile-image')[0]
	}

	// Methodes d'API publiques

	setLarger(islarger) {
		this.classList[islarger ? "add" : "remove"]("is-larger")
	}

	setCurrent(isCurrent) {
		this.classList[isCurrent ? "add" : "remove"]("is-current")
	}

	setTitle(title) {
		this.titleLabel = title
	}

	setImage(image) {
		this.image = image
	}

	setDescription(description) {
		this.description = description
	}

	setVisible(isVisible) {
		this.classList[isVisible ? "add" : "remove"]("is-visible")
	}

}

window.customElements.define('home-tile', HomeTile)