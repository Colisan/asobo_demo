class HomeSubMenu extends HTMLElement {
	constructor() {
			super()
	}

	connectedCallback() {
		this.update_content()
	}

	update_content() {
		this.innerHTML = "";
		let currentSubMenu = window.fakeDataManager.menu.filter(e => e.current)[0].subMenu
		for (let menuDatas of currentSubMenu) {
			let menuItem = document.createElement("home-menu-item")
			// Une autre option aurait été de passer entièrement menuDatas
			// au menuItem et de le laisser se débrouiller pour les données/events
			menuItem.setLabel(menuDatas.label)
			menuItem.setCurrent(menuDatas.current)
			menuItem.setLarger(menuDatas.larger)
			menuItem.setSmaller(true)
			menuItem.addEventListener("click", () => {
				window.fakeDataManager.setCurrentSubMenu(menuDatas)
			})
			this.appendChild(menuItem)
		}
	}
}

window.customElements.define('home-sub-menu', HomeSubMenu)