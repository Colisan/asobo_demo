class HomeTileList extends HTMLElement {
	constructor() {
			super()
	}

	connectedCallback() {
		this.update_content()
	}

	update_content() {
		this.innerHTML = ""
		let createdTiles = []

		// Etape 1 : Création des tuiles
		for (let tileDatas of window.fakeDataManager.tiles) {
			let tile = document.createElement("home-tile")
			tile.setTitle(tileDatas.title)
			tile.setImage(tileDatas.image)
			tile.setDescription(tileDatas.description)
			tile.setLarger(tileDatas.larger)
			tile.setCurrent(tileDatas.current)
			createdTiles.push(tile)
			this.appendChild(tile)
		}
		 
		// Etape 1 : Apparition progressive des tuiles
		for (let newTileIndex in createdTiles) {
			window.setTimeout(() => {
				createdTiles[newTileIndex].setVisible(true)
			}, newTileIndex * 150)
		}
	}
}

window.customElements.define('home-tile-list', HomeTileList)