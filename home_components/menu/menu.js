class HomeMenu extends HTMLElement {
	constructor() {
			super()
	}

	connectedCallback() {
		this.update_content()
	}

	update_content() {
		this.innerHTML = "";
		for (let menuDatas of window.fakeDataManager.menu) {
			let menuItem = document.createElement("home-menu-item")
			menuItem.setLabel(menuDatas.label)
			menuItem.setCurrent(menuDatas.current)
			menuItem.addEventListener("click", () => {
				window.fakeDataManager.setCurrentMenu(menuDatas)
			})
			this.appendChild(menuItem)
	 	}
	}
}

window.customElements.define('home-menu', HomeMenu)