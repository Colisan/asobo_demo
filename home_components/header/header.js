class HomeHeader extends HTMLElement {
	constructor() {
			super()
			this.menuElt = undefined
			this.subMenuElt = undefined
	}

	connectedCallback() {
		this.update_content()
	}

	update_content() {
		// Pas besoin de recréer les sous-composants à chaque fois !
		// S'ils sont déjà existant, on transfert juste la demande d'update
		
		if (!this.menuElt) {
			this.menuElt = document.createElement("home-menu")
			this.appendChild(this.menuElt)
		}
		else this.menuElt.update_content()

		if (!this.subMenuElt) {
			this.subMenuElt = document.createElement("home-sub-menu")
			this.appendChild(this.subMenuElt)
		}
		else this.subMenuElt.update_content()
	}
}

window.customElements.define('home-header', HomeHeader)