class HomeMenuItem extends HTMLElement {
	constructor() {
			super()
			this.label = undefined
	}

	connectedCallback() {
		this.update_content()
		addRippleEffect(this)
	}

	update_content() {
		this.innerHTML = "";
		this.innerHTML = this.label
	}

	// Methodes d'API publiques

	setSmaller(isSmaller) {
		this.classList[isSmaller ? "add" : "remove"]("is-smaller")
	}

	setLarger(isLarger) {
		this.classList[isLarger ? "add" : "remove"]("is-larger")
	}

	setLabel(label) {
		this.label = label
	}

	setCurrent(isCurrent) {
		this.classList[isCurrent ? "add" : "remove"]("is-current")
	}
}

window.customElements.define('home-menu-item', HomeMenuItem)