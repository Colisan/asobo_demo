class AsoboHome extends HTMLElement {
	constructor() {
			super()
			this.headerElt = undefined
			this.tileListElt = undefined
	}

	connectedCallback() {
		this.update_content()
	}

	update_content() {
		// Pas besoin de recréer les sous-composants à chaque fois !
		// S'ils sont déjà existant, on transfert juste la demande d'update
		
		if (!this.headerElt) {
			this.headerElt = document.createElement("home-header")
			this.appendChild(this.headerElt)
		}
		else this.headerElt.update_content()

		if (!this.tileListElt) {
			// Un petite timeout sur le premier chargement des tuiles pour faire classe =)
			window.setTimeout(() => {
				this.tileListElt = document.createElement("home-tile-list")
				this.appendChild(this.tileListElt)
			}, 500)
		}
		else this.tileListElt.update_content()
	}
}

window.customElements.define('asobo-home', AsoboHome)